# Hello CP3408 - Project Info

Created in Unity 2021.3.18f1

## Prerequisites
* Install [git](https://git-scm.com/)
* Install [git-lfs](https://git-lfs.com/)
* Setup your [GitLab account](https://docs.gitlab.com/ee/tutorials/)

**Note: GitLab terminology for a repo is "project"**

## How to setup your Unity project on GitLab: 
1. On your local machine, create the project in [Unity](https://unity.com/)
1. On your local macine in the Unity project folder, create .gitignore and .gitattributes from [resources](#resources-used-to-make-this-infobyte)
1. On GitLab, create a new ["blank" GitLab project](https://docs.gitlab.com/ee/user/project/#create-a-blank-project) - don't set a deployment target, don't add README, don't enable SAST
1. On GitLab, make sure that you can [push to the main branch](https://docs.gitlab.com/ee/user/project/protected_branches.html#configure-a-protected-branch) - when working in a small team enable merge on the main branch rather than enable push!
1. On your local machine, open Windows Terminal (or similar) in the project folder and type the following commands:
```
git init -b main
git add .
git commit -m "Initial commit"
git config --global user.name "YOUR NAME"
git config --global user.email "YOUR EMAIL"
git remote add origin https://gitlab.com/.../YOURPROJECTNAME.git
git push -u origin main
```

You can then type in `git status` and you should see:

> On branch main  
> Your branch is up to date with 'origin/main'.

After that, you are free to update your Unity project locally and push changes to remote GitLab project!

## Beware

Prefer leaving the `.gitattributes` alone! But, if you need to change `.gitattributes` for some reason, then all files must be un-staged and re-staged for changes to the LFS to be applied properly:
```
git reset HEAD --
git add -A
```

Changes can then be verified using: `git lfs status`

## Resources used to make this infobyte
* [Getting started with Git LFS](https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/)
* [gitignore for Unity projects](https://github.com/github/gitignore/blob/main/Unity.gitignore)
* [gitattributes for Unity projects](https://hextantstudios.com/unity-gitattributes/)
* [Track LFS as early as possible](https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/#track-as-early-as-possible)
* [Windows terminal](https://apps.microsoft.com/store/detail/windows-terminal/9N0DX20HK701?hl=en-au&gl=au&rtc=1)
* [Git remote infobyte](https://www.atlassian.com/git/tutorials/syncing)
* [Gitlab LFS](https://docs.gitlab.com/ee/topics/git/lfs/index.html#git-large-file-storage-lfs)
