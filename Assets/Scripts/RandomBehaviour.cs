using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomBehaviour : MonoBehaviour
{
    private new Rigidbody rigidbody;
    private Vector3 randomiser;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        randomiser = Vector3.zero;
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor")) return; // ignore the Floor!

        // otherwise adjust the velocity of this gameobject slightly
        randomiser.x = ComputeSmallOffsetValue();
        randomiser.y = ComputeSmallOffsetValue();
        randomiser.z = ComputeSmallOffsetValue();

        rigidbody.velocity += randomiser;

        Debug.Log($"OnCollisionExit called - {name} velocity adjusted slightly by {randomiser}");
    }

    private static float ComputeSmallOffsetValue()
    {
        return Random.value * (Random.value < 0.5f ? -1.0f : 1.0f) * 0.1f;
    }
}

